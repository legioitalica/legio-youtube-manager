﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using log4net.Repository.Hierarchy;
using Microsoft.Win32;

namespace LYTM.App {

    /// <summary>
    /// Logica di interazione per App.xaml
    /// </summary>
    public partial class App : Application {

        protected override void OnStartup (StartupEventArgs e) {
            Bugsnag.Clients.WPFClient.Start ();
            Bugsnag.Clients.WPFClient.Config.AppVersion = Version.informationalFull;
            Bugsnag.Clients.WPFClient.Config.StoreOfflineErrors = true;

            System.Threading.Thread.CurrentThread.Name = "Legio YouTube Manager Main Thread";

            base.OnStartup (e);
        }

        private void Application_Startup (object sender, StartupEventArgs e) {
            String configName = Core.Utility.getConfigFileName ();
            if (!System.IO.File.Exists (configName))
                using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly ().GetManifestResourceStream (@"LYTM.App.Resources.config.json")) {
                    using (System.IO.FileStream fileStream = new System.IO.FileStream (configName, System.IO.FileMode.Create)) {
                        for (int i = 0; i < stream.Length; i++) {
                            fileStream.WriteByte ((byte) stream.ReadByte ());
                        }
                        fileStream.Close ();
                    }
                }
            Impostazioni.Impostazioni imp = new Impostazioni.Impostazioni ();
            String tema = imp.ImpostazioniApplicazione.Tema;
            String accent = imp.ImpostazioniApplicazione.Accent;
            MahApps.Metro.ThemeManager.ChangeAppStyle (Application.Current, MahApps.Metro.ThemeManager.GetAccent (accent), MahApps.Metro.ThemeManager.GetAppTheme (tema));
            Application.Current.Properties["Impostazioni"] = imp;

            ////Configurazione Log
            log4net.GlobalContext.Properties["Assembly.Version"] = System.Diagnostics.FileVersionInfo.GetVersionInfo (System.Reflection.Assembly.GetExecutingAssembly ().Location).FileVersion;
            System.Management.ManagementObjectSearcher mos = new System.Management.ManagementObjectSearcher ("select * from Win32_OperatingSystem");
            foreach (System.Management.ManagementObject managementObject in mos.Get ()) {
                log4net.GlobalContext.Properties["WindowsVersion"] = managementObject["Caption"].ToString ();   //Display operating system caption
                log4net.GlobalContext.Properties["OSArchitecture"] = managementObject["OSArchitecture"].ToString ();   //Display operating system architecture.
                if (managementObject["CSDVersion"] != null)
                    log4net.GlobalContext.Properties["CSDVersion"] = managementObject["CSDVersion"].ToString ();     //Display operating system version.
                else
                    log4net.GlobalContext.Properties["CSDVersion"] = "";
                log4net.GlobalContext.Properties["RAM"] = Core.Utility.sizeSuffix (Convert.ToInt64 (managementObject["TotalVisibleMemorySize"].ToString ()) * 1024, 2);
            }

            RegistryKey processor_name = Registry.LocalMachine.OpenSubKey (@"Hardware\Description\System\CentralProcessor\0", RegistryKeyPermissionCheck.ReadSubTree);   //This registry entry contains entry for processor info.
            if (processor_name != null) {
                if (processor_name.GetValue ("ProcessorNameString") != null) {
                    log4net.GlobalContext.Properties["Processore"] = processor_name.GetValue ("ProcessorNameString");   //Display processor info.
                }
            }
            log4net.GlobalContext.Properties["logFile"] = Core.Utility.getLogFileName ();
            log4net.Config.XmlConfigurator.Configure ();

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler (MyHandler);

            Hierarchy hier = (Hierarchy) log4net.LogManager.GetRepository ();
            switch (imp.ImpostazioniApplicazione.LogLevel) {
                case 0:
                    hier.Root.Level = log4net.Core.Level.All;
                    break;

                case 1:
                    hier.Root.Level = log4net.Core.Level.Debug;
                    break;

                case 2:
                    hier.Root.Level = log4net.Core.Level.Info;
                    break;

                case 3:
                    hier.Root.Level = log4net.Core.Level.Warn;
                    break;

                case 4:
                    hier.Root.Level = log4net.Core.Level.Error;
                    break;

                case 5:
                    hier.Root.Level = log4net.Core.Level.Fatal;
                    break;

                default:
                    break;
            }

            if (imp.ImpostazioniApplicazione.BugsnagUserInfo)
                Bugsnag.Clients.WPFClient.Config.SetUser (Core.Utility.UniqueIDStr, imp.ImpostazioniApplicazione.BugsnagMail, imp.ImpostazioniApplicazione.BugsnagNome);

            Bugsnag.Clients.WPFClient.SendStoredReports ();

            Core.Core.Instance.Avvia ();
        }

        private void MyHandler (object sender, UnhandledExceptionEventArgs e) {
            log4net.ILog logger = log4net.LogManager.GetLogger (typeof (App));

            Exception ex = (Exception) e.ExceptionObject;
            logger.Fatal ("Unhandled Exception", ex);
            logger.Fatal (String.Format ("Runtime terminating: {0}", e.IsTerminating));

            if (e.IsTerminating) {
                Core.Core.Instance.Arresta ();

                String footer = @"#-----------------------------------------------------------
# Codice Uscita: %property{CodiceUscita}
# Max Ram Usata: %property{RAM.Usata}
# Tempo Processore Totale: %property{TempoProcessore}
#-----------------------------------------------------------
";

                System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess ();

                footer = footer.Replace ("%property{RAM.Usata}", Core.Utility.sizeSuffix (proc.PeakWorkingSet64, 2));
                footer = footer.Replace ("%property{TempoProcessore}", proc.TotalProcessorTime.ToString ());

                if (ex is Core.Exceptions.BaseException)
                    footer = footer.Replace ("%property{CodiceUscita}", "0x" + ((Core.Exceptions.BaseException) ex).ErrorCode.ToString ("X8"));
                else
                    footer = footer.Replace ("%property{CodiceUscita}", "0x" + (Exceptions.ErrorCode.UnknownError.ToString ("X8")));

                Hierarchy hier = log4net.LogManager.GetRepository () as Hierarchy;
                if (hier != null) {
                    var appender = (ZipFileAppender) hier.GetAppenders ().Where (app => app.Name.Equals ("LogFileAppender", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault ();
                    appender.Layout = new log4net.Layout.PatternLayout ("%message %newline") { Footer = footer };
                    logger.Info (footer);
                }
            }
            Bugsnag.Clients.WPFClient.Notify (ex);
        }

        private void Application_Exit (object sender, ExitEventArgs e) {
            Core.Core.Instance.Arresta ();

            String footer = @"#-----------------------------------------------------------
# Codice Uscita: %property{CodiceUscita}
# Max Ram Usata: %property{RAM.Usata}
# Tempo Processore Totale: %property{TempoProcessore}
#-----------------------------------------------------------
";

            System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess ();

            footer = footer.Replace ("%property{RAM.Usata}", Core.Utility.sizeSuffix (proc.PeakWorkingSet64, 2));
            footer = footer.Replace ("%property{TempoProcessore}", proc.TotalProcessorTime.ToString ());
            footer = footer.Replace ("%property{CodiceUscita}", "0x" + e.ApplicationExitCode.ToString ("X8"));

            Hierarchy hier = log4net.LogManager.GetRepository () as Hierarchy;
            if (hier != null) {
                var appender = (ZipFileAppender) hier.GetAppenders ().Where (app => app.Name.Equals ("LogFileAppender", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault ();
                appender.Layout = new log4net.Layout.PatternLayout ("% date[% thread] % -5level % logger - % message % newline") { Footer = footer };
            }
        }
    }
}