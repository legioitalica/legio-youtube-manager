﻿namespace LYTM.App.Exceptions {

    internal class ErrorCode {
        public const int NoError = 0x00000000;
        public const int UnknownError = 0x000FFFFFF;
    }
}