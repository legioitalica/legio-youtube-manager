﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace LYTM.App.UI.Helpers {

    internal class HelperGruppoMenu : IValueConverter {

        public Object Convert (Object value, Type targetType, Object parameter, CultureInfo culture) {
            Core.Plugin.Categoria cat = (Core.Plugin.Categoria) value;
            var imp = new Impostazioni.Impostazioni ();
            String tema = imp.ImpostazioniApplicazione.Tema;
            switch (cat) {
                case Core.Plugin.Categoria.YouTube:
                    return App.Current.Resources["YouTube"];

                case Core.Plugin.Categoria.Miniature:
                    return "";

                case Core.Plugin.Categoria.Video:
                    return "";

                default:
                    return "";
            }
        }

        public Object ConvertBack (Object value, Type targetType, Object parameter, CultureInfo culture) {
            throw new NotImplementedException ();
        }
    }
}