﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace LYTM.App.UI.Helpers {

    internal class FlyoutWidthHelper : IValueConverter {

        public Object Convert (Object value, Type targetType, Object parameter, CultureInfo culture) {
            double ret;
            if (Double.TryParse (value.ToString (), out ret))
                return ret * 0.25;
            return value;
        }

        public Object ConvertBack (Object value, Type targetType, Object parameter, CultureInfo culture) {
            throw new NotImplementedException ();
        }
    }
}