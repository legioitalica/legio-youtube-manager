﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Data;
using log4net;
using LYTM.Core.Plugin;
using MahApps.Metro.SimpleChildWindow;

namespace LYTM.App.UI.Finestre {

    /// <summary>
    /// Logica di interazione per Finestra_Principale.xaml
    /// </summary>
    public partial class FinestraPrincipale : MahApps.Metro.Controls.MetroWindow {
        private ILog logger = LogManager.GetLogger (typeof (FinestraPrincipale));
        public ObservableCollection<Core.Plugin.IPlugin> Plugins { get; private set; }

        private MahApps.Metro.SimpleChildWindow.ChildWindow child;

        public FinestraPrincipale () {
            Plugins = new ObservableCollection<IPlugin> ();
            InitializeComponent ();

            CollectionView view = (CollectionView) CollectionViewSource.GetDefaultView (ListaMenu.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription ("Categoria");
            view.GroupDescriptions.Add (groupDescription);
        }

        #region Eventi UI

        private void Bitbucket_Click (object sender, RoutedEventArgs e) {
            System.Diagnostics.Process.Start ("https://bitbucket.org/legioitalica/legio-youtube-manager");
        }

        private async void Impostazioni_Click (object sender, RoutedEventArgs e) {
            if (child != null && child.IsOpen)
                return;
            child = new Impostazioni () { IsModal = true, AllowMove = false };
            await this.ShowChildWindowAsync (child);
        }

        private async void About_Click (object sender, RoutedEventArgs e) {
            if (child != null && child.IsOpen)
                return;
            child = new InformazioniSu () { IsModal = true, AllowMove = false };
            await this.ShowChildWindowAsync (child);
        }

        private void PayPal_Click (object sender, RoutedEventArgs e) {
            System.Diagnostics.Process.Start (ApiKeys.PayPalLink);
        }

        private void Menu_Click (object sender, RoutedEventArgs e) {
            FlyoutMenu.IsOpen = !FlyoutMenu.IsOpen;
        }

        private void MetroWindow_Loaded (object sender, RoutedEventArgs e) {
        }

        #endregion Eventi UI
    }
}