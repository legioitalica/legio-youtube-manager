﻿using System;
using System.Collections.Generic;

namespace LYTM.App.Impostazioni {

    internal class Impostazioni : FX.Configuration.JsonConfiguration {

        public Impostazioni () : base (Core.Utility.getConfigFileName ()) {
        }

        public ImpostazioniApp ImpostazioniApplicazione { get; set; }
        public Core.Impostazioni.ImpostazioniCore ImpostazioniCore { get; set; }
        public Dictionary<String, Core.Impostazioni.IImpostazioni> ImpostazioniPlugin { get; set; }
    }
}