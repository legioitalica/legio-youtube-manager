﻿using System;

namespace LYTM.App.Impostazioni {

    internal class ImpostazioniApp : Core.Impostazioni.IImpostazioni {

        public event EventHandler ImpostazioniAggiornate;

        private String _tema, _accent, _bugsnagNome, _bugsnagMail;
        private short _logLevel;
        private bool _bugsnagUserInfo;

        public String Tema {
            get {
                return _tema;
            }
            set {
                _tema = value;
                OnPropertyChanged ("Tema");
            }
        }

        public String Accent {
            get {
                return _accent;
            }
            set {
                _accent = value;
                OnPropertyChanged ("Accent");
            }
        }

        public short LogLevel {
            get {
                return _logLevel;
            }
            set {
                _logLevel = value;
                OnPropertyChanged ("LogLevel");
            }
        }

        public bool BugsnagUserInfo {
            get {
                return _bugsnagUserInfo;
            }
            set {
                _bugsnagUserInfo = value;
                OnPropertyChanged ("Bugsnag");
            }
        }

        public String BugsnagNome {
            get {
                return _bugsnagNome;
            }
            set {
                _bugsnagNome = value;
                OnPropertyChanged ("Bugsnag");
            }
        }

        public String BugsnagMail {
            get {
                return _bugsnagMail;
            }
            set {
                _bugsnagMail = value;
                OnPropertyChanged ("Bugsnag");
            }
        }

        protected void OnPropertyChanged (String name) {
            if (ImpostazioniAggiornate != null) {
                ImpostazioniAggiornate (this, new System.ComponentModel.PropertyChangedEventArgs (name));
            }
        }
    }
}