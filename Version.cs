namespace LYTM.App {

    internal class Version {
        public static readonly System.Version number = new System.Version (1,0,0,48);
        public const string numberWithRevString = "1.0.0.48";
        public const string numberString = "1.0.08";
        public const string branchName = "develop";
        public const string branchSha1 = "a2f76bf";
        public const string branchRevCount = "7";
        public const string informational = "1.0.0.48 [ a2f76bf ]";
        public const string informationalFull = "1.0.0.48 [ a2f76bf ] /'develop':7";
    }
}
