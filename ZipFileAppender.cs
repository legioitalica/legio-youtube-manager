﻿using System;
using System.Linq;

namespace LYTM.App {

    public class ZipFileAppender : log4net.Appender.RollingFileAppender {

        protected override void AdjustFileBeforeAppend () {
            base.AdjustFileBeforeAppend ();
            if (System.IO.File.Exists (Core.Utility.getLogFileName () + ".1")) { // zip the file
                using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile (Core.Utility.getLogZipFileName ())) {
                    long elementiPresenti = zip.LongCount ();
                    String newName = System.IO.Path.Combine (System.IO.Path.GetDirectoryName (Core.Utility.getLogFileName ()), System.IO.Path.GetFileNameWithoutExtension (Core.Utility.getLogFileName ())) + "." + elementiPresenti + ".log";
                    System.IO.File.Move (Core.Utility.getLogFileName () + ".1", newName);
                    zip.AddFile (Core.Utility.getLogFileName () + "." + elementiPresenti, "");
                    zip.Save ();
                    System.IO.File.Delete (newName);
                }
            }
        }
    }
}