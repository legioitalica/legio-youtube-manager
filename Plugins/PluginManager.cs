﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.App.Plugins {

    internal class PluginManager {
        public static System.Collections.ObjectModel.ObservableCollection<Core.Plugin.IPlugin> Plugins { get; private set; }
        public static Dictionary<String, Core.Plugin.IDownloaderPlugin> DownloadPlugins { get; private set; }
        private static log4net.ILog logger = log4net.LogManager.GetLogger (typeof (PluginManager));

        static PluginManager () {
            Plugins = new System.Collections.ObjectModel.ObservableCollection<Core.Plugin.IPlugin> ();
            DownloadPlugins = new Dictionary<string, Core.Plugin.IDownloaderPlugin> ();

            logger.Info ("Cerco e carico plugins");

            String[] pluginsDir = System.IO.Directory.GetDirectories (Core.Utility.getPluginDir ());
            foreach (var plugin in pluginsDir) {
                bool caricato = false;
                var nomeDll = System.IO.Path.GetDirectoryName (plugin);
                logger.InfoFormat ("Carico plugin in subfolder {0}", nomeDll);

                nomeDll = System.IO.Path.Combine (plugin, nomeDll + ".dll");
                try {
                    System.Reflection.Assembly asm = System.Reflection.Assembly.Load (nomeDll);
                    if (asm != null) {
                        var tipo = asm.GetType (asm.FullName);
                        if (tipo != null && typeof (Core.Plugin.IPlugin).IsAssignableFrom (tipo)) {
                            var pluginRef = (Core.Plugin.IPlugin) Activator.CreateInstance (tipo);
                            Plugins.Add (pluginRef);
                            logger.InfoFormat ("Plugin {0} è stato caricato correttamente", pluginRef.Nome);
                            caricato = true;
                        }
                        if (tipo != null && typeof (Core.Plugin.IDownloaderPlugin).IsAssignableFrom (tipo)) {
                            var pluginRef = (Core.Plugin.IDownloaderPlugin) Activator.CreateInstance (tipo);
                            var listaUrl = pluginRef.UrlSupportate;
                            foreach (String url in listaUrl)
                                DownloadPlugins[url] = pluginRef;
                            logger.InfoFormat ("Plugin {0} caricato; si occuperà del download per siti: {1}", pluginRef.Nome, String.Join (",", pluginRef.UrlSupportate));
                        }
                    }
                } catch (Exception e) {
                    logger.Error (e);
                }
                if (!caricato)
                    logger.InfoFormat ("Plugin non presente in {0}", plugin);
            }
        }
    }
}