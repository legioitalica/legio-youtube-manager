﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.App.Plugins {

    internal class Repository {
        public String Nome { get; set; }

        public String UrlIcona { get; set; }
        public Dictionary<String, Plugin> Plugins { get; set; }
    }
}