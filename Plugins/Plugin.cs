﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LYTM.App.Plugins {

    internal class Plugin {
        public String Descrizione { get; set; }
        public List<String> Autori { get; set; }
        public System.Version Versione { get; set; }
        public String DownloadUrl { get; set; }
    }
}